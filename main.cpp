#include <iostream>
#include <lldb/API/LLDB.h>

using namespace std;

void log(const char *msg, void *) {
  cout << "lldb-example log: " << msg << endl;
}

int main() {
  lldb::SBDebugger::Initialize();

  lldb::SBDebugger debugger(lldb::SBDebugger::Create(true, log, nullptr));
  lldb::SBListener listener("lldb-example listener");

  auto target = debugger.CreateTarget("test");
  if (!target.IsValid()) {
    cerr << "could not create target" << endl;
    return 1;
  }

  auto breakpoint = target.BreakpointCreateByLocation("test.cpp", 3);
  if (!breakpoint.IsValid()) {
    cerr << "could not create breakpoint" << endl;
    return 1;
  }

  vector<const char *> argv;
  argv.emplace_back(nullptr);
  vector<const char *> environment;
  environment.emplace_back(nullptr);

  lldb::SBError error;
  // Working directory set to / for simplicity
  lldb::SBProcess process(target.Launch(listener, argv.data(), environment.data(), nullptr, nullptr, nullptr, "/", lldb::eLaunchFlagNone, false, error));
  if (error.Fail()) {
    cerr << "could not create process: " << error.GetCString() << endl;
    return 1;
  }

  lldb::SBEvent event;
  while (true) {
    if (listener.GetNextEvent(event)) {
      if ((event.GetType() & lldb::SBProcess::eBroadcastBitStateChanged) > 0) {
        auto variables = process.GetThreadAtIndex(0).GetFrameAtIndex(0).GetVariables(true, true, true, true);
        for (uint32_t value_index = 0; value_index < variables.GetSize(); value_index++) {
          auto value = variables.GetValueAtIndex(value_index);
          auto type = value.GetType();
          cout << "type: " << value.GetDisplayTypeName() << endl;

          auto data = value.GetData();
          cout << "size: " << data.GetByteSize() << endl;

          auto int32 = data.GetSignedInt32(error, 0); // get value as int (32 bit (4 byte) on 64-bit systems)
          if (error.Fail()) {
            cout << "could not get int32: " << error.GetCString() << endl;
            return 1;
          }
          cout << "data as int32: " << int32 << endl;
        }
      }
    }
  }
}
