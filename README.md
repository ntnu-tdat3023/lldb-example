# LLDB-example
The lldb-example starts debugging the test binary created from test.cpp. A
breakpoint is added, and when reached, outputs the variable content. On Linux,
the variable content will output several times.

## Prerequisite
Linux

### ArchLinux based distributions:
```sh
sudo pacman -S lldb
```

### Debian-based distributions:
```sh
sudo apt-get install liblldb-dev
```

## Run example
The example has to be run in a terminal:
```sh
mkdir build && cd build
cmake ..
make && ./lldb_example
```
